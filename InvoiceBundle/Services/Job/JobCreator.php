<?php

namespace InvoiceBundle\Services\Job;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\EntityManager\WorkorderManager;
use AppBundle\Entity\Workorder;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Factories\JobFactory;

class JobCreator
{
    /** @var WorkorderManager */
    private $workorderManager;

    public function __construct(WorkorderManager $workorderManager)
    {
        $this->workorderManager = $workorderManager;
    }

    /**
     * @param Workorder $workorder
     * @return \InvoiceBundle\Entity\Job
     */
    public function createBlankJob(Workorder $workorder)
    {
        /** @var AccountContactPerson $ACP */
        $ACP = $this->workorderManager->getWorkorderAccountAuthorizer($workorder);
        /** @var Customer $customer */
        $customer = $ACP->getCustomer();

        return JobFactory::make($workorder->getAccount(), $customer, $workorder->getDivision());
    }
}