<?php

namespace InvoiceBundle\Services\Job;

use InvoiceBundle\Entity\Job;
use QuickbooksBundle\Services\Quickbooks\Job\CreatorJob;
use QuickbooksBundle\Services\Quickbooks\Job\ModifierJob;

class JobHandler
{
    /** @var JobEditor */
    private $jobEditor;
    /** @var CreatorJob */
    private $creatorJobInQB;
    /** @var ModifierJob */
    private $modifierJob;

    /**
     * JobHandler constructor.
     * @param JobEditor $jobEditor
     * @param CreatorJob $creatorJobInQB
     * @param ModifierJob $modifierJob
     */
    public function __construct(JobEditor $jobEditor, CreatorJob $creatorJobInQB, ModifierJob $modifierJob)
    {
        $this->jobEditor = $jobEditor;
        $this->creatorJobInQB = $creatorJobInQB;
        $this->modifierJob = $modifierJob;
    }

    /**
     * @param Job $job
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendJobToAccountingSystem(Job $job)
    {
        if (empty($job->getAccountingSystemId())) {
            /** @var Job $job */
            $jobFromAccountingSystem = $this->creatorJobInQB->createJob($job);
            $this->jobEditor->addAccountingSystemId($job, $jobFromAccountingSystem);
        } else {
            $jobFromAccountingSystem = $this->modifierJob->modifyJob($job);
            $this->jobEditor->addAccountingSystemId($job, $jobFromAccountingSystem);
        }
    }
}