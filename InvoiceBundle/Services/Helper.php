<?php

namespace InvoiceBundle\Services;

class Helper
{
    /**
     * @param object|null $value
     * @param array $array
     * @param string $key
     */
    public static function addToArrayIfNotNull($value, array &$array, $key)
    {
        if (!empty($value)) {
            $array[$key] = $value;
        }
    }
}