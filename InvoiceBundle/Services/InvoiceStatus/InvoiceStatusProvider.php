<?php

namespace InvoiceBundle\Services\InvoiceStatus;

use InvoiceBundle\Entity\InvoiceStatus;
use InvoiceBundle\Repository\InvoiceStatusRepository;

class InvoiceStatusProvider
{
    /** @var InvoiceStatusRepository */
    private $invoiceStatusRepository;

    /**
     * InvoiceStatusProvider constructor.
     */
    public function __construct(InvoiceStatusRepository $invoiceStatusRepository)
    {
        $this->invoiceStatusRepository = $invoiceStatusRepository;
    }

    /**
     * @return InvoiceStatus|null
     */
    public function getDraft()
    {
        /** @var InvoiceStatus $status */
        $status = $this->invoiceStatusRepository->findOneBy(['alias' => InvoiceStatus::STATUS_DRAFT]);

        return $status;
    }

    /**
     * @return InvoiceStatus
     */
    public function getInQB()
    {
        /** @var InvoiceStatus $status */
        $status = $this->invoiceStatusRepository->findOneBy(['alias' => InvoiceStatus::STATUS_IN_QB]);

        return $status;
    }
}