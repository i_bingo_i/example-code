<?php

namespace InvoiceBundle\Services\Customer\Provider;

use AppBundle\Entity\AccountContactPerson;
use InvoiceBundle\Services\Helper;

class CustomerACPProvider
{
    /** @var CustomerCompanyProvider */
    private $customerCompanyProvider;
    /** @var CustomerContactPersonProvider */
    private $customerContactPersonProvider;

    public function __construct(
        CustomerCompanyProvider $customerCompanyProvider,
        CustomerContactPersonProvider $customerContactPersonProvider
    ) {
        $this->customerCompanyProvider = $customerCompanyProvider;
        $this->customerContactPersonProvider = $customerContactPersonProvider;
    }

    /**
     * @param AccountContactPerson $accountContactPerson
     * @return array
     */
    public function getCustomers(AccountContactPerson $accountContactPerson)
    {
        $res = [];

        if (!empty($accountContactPerson->getContactPerson())) {
            Helper::addToArrayIfNotNull(
                $this->customerContactPersonProvider->getCustomer($accountContactPerson->getContactPerson()),
                $res,
                'Account Person'
            );

            if (!empty($accountContactPerson->getContactPerson()->getCompany())) {
                Helper::addToArrayIfNotNull(
                    $this->customerCompanyProvider->getCustomer(
                        $accountContactPerson->getContactPerson()->getCompany()
                    ),
                    $res,
                    'Person Company'
                );
            }
        }

        if (!empty($accountContactPerson->getAccount()->getCompany())) {
            Helper::addToArrayIfNotNull(
                $this->customerCompanyProvider->getCustomer(
                    $accountContactPerson->getAccount()->getCompany()
                ),
                $res,
                'Account Company'
            );
        }

        return $res;
    }
}