<?php

namespace InvoiceBundle\Services\Customer\Provider;

use InvoiceBundle\Entity\EntityType;
use InvoiceBundle\Repository\CustomerRepository;
use InvoiceBundle\Repository\EntityTypeRepository;

class BaseCustomerProvider
{
    /** @var CustomerRepository */
    protected $customerRepository;
    /** @var EntityTypeRepository */
    protected $entityTypeRepository;
    /** @var EntityType */
    protected $entityType;

    /**
     * BaseCustomerProvider constructor.
     * @param CustomerRepository $customerRepository
     * @param EntityTypeRepository $entityTypeRepository
     */
    public function __construct(CustomerRepository $customerRepository, EntityTypeRepository $entityTypeRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->entityTypeRepository = $entityTypeRepository;
    }

    /**
     * @param int $entityId
     * @return null|object
     */
    protected function getByEntityId($entityId)
    {
        return $this->customerRepository->findOneBy(
            [
                'entityId' => $entityId,
                'entityType' => $this->entityType
            ]
        );
    }
}