<?php

namespace InvoiceBundle\Services\Customer\Provider;

use AppBundle\Entity\ContactPerson;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Entity\EntityType;
use InvoiceBundle\Repository\CustomerRepository;
use InvoiceBundle\Repository\EntityTypeRepository;

class CustomerContactPersonProvider extends BaseCustomerProvider
{
    /**
     * CustomerContactPersonProvider constructor.
     * @param CustomerRepository $customerRepository
     * @param EntityTypeRepository $entityTypeRepository
     */
    public function __construct(CustomerRepository $customerRepository, EntityTypeRepository $entityTypeRepository)
    {
        parent::__construct($customerRepository, $entityTypeRepository);

        $this->entityType = $this->entityTypeRepository->findOneBy(['alias' => EntityType::TYPE_CONTACT_PERSON]);
    }

    /**
     * @param ContactPerson $contactPerson
     * @return Customer
     */
    public function getCustomer(ContactPerson $contactPerson)
    {
        /** @var Customer $customer */
        $customer = parent::getByEntityId($contactPerson->getId());

        return $customer;
    }
}