<?php

namespace InvoiceBundle\Services\Customer;

use AppBundle\Entity\Address;
use AppBundle\Entity\AddressType;
use AppBundle\Entity\Company;
use AppBundle\Entity\ContactPerson;
use AppBundle\Services\Address\Creator;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Services\Customer\Editor\CustomerEditor;
use QuickbooksBundle\Services\Quickbooks\Customer\CreatorCustomer;
use InvoiceBundle\Services\Customer\Creator\CustomerCompanyCreator;
use InvoiceBundle\Services\Customer\Creator\CustomerContactPersonCreator;
use QuickbooksBundle\Services\Quickbooks\Customer\ModifierCustomer;

class CustomerHandler
{
    /** @var CreatorCustomer */
    private $creatorCustomer;
    /** @var CustomerEditor */
    private $customerEditor;
    /** @var CustomerCompanyCreator */
    private $customerCompanyCreator;
    /** @var CustomerContactPersonCreator */
    private $customerContactPersonCreator;
    /** @var Creator */
    private $addressCreator;

    /**
     * CustomerHandler constructor.
     * @param CreatorCustomer $creatorCustomer
     * @param CustomerEditor $customerEditor
     * @param CustomerCompanyCreator $customerCompanyCreator
     * @param CustomerContactPersonCreator $customerContactPersonCreator
     * @param Creator $addressCreator
     */
    public function __construct(
        CreatorCustomer $creatorCustomer,
        CustomerEditor $customerEditor,
        CustomerCompanyCreator $customerCompanyCreator,
        CustomerContactPersonCreator $customerContactPersonCreator,
        Creator $addressCreator
    ) {
        $this->creatorCustomer = $creatorCustomer;
        $this->customerEditor = $customerEditor;
        $this->customerCompanyCreator = $customerCompanyCreator;
        $this->customerContactPersonCreator = $customerContactPersonCreator;
        $this->addressCreator = $addressCreator;
    }

    /**
     * @param Customer $customer
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendCustomerToAccountingSystem(Customer $customer)
    {
        if (empty($customer->getAccountingSystemId())) {
            $customerFromAccountingSystem = $this->creatorCustomer->createCustomer($customer);
            $this->customerEditor->setAccountingSystemId($customer, $customerFromAccountingSystem);
        }
    }

    /**
     * @param $entity
     * @param Customer|null $customer
     * @param Address|null $address
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createOrUpdateCustomer($entity, ?Customer $customer, ?Address $address)
    {
        if (empty($customer)) {
            $this->creatingCustomer($entity, $address);
        } elseif (!empty($address)) {
            $this->prepareBillingAddress($address);
            $this->customerEditor->modify($customer, $address, $this->prepareCustomerName($entity));
        }
    }

    /**
     * @param $entity
     * @param Address|null $address
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function creatingCustomer($entity, ?Address $address)
    {
        $this->prepareBillingAddress($address);

        if ($entity instanceof Company) {
            $this->customerCompanyCreator->createCustomer($entity, $address);

        } elseif ($entity instanceof ContactPerson) {
            $this->customerContactPersonCreator->createCustomer($entity, $this->prepareCustomerName($entity), $address);
        }
    }

    /**
     * @param Customer|null $customer
     * @param Address|null $address
     * @param $isCustom
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setCustomBillingAddress(?Customer $customer, ?Address $address, $isCustom)
    {
        if ($isCustom && !empty($customer) && !empty($address)) {
            $this->customerEditor->setCustomBillingAddress($customer, $address);
        }
    }

    /**
     * @param Address|null $address
     */
    private function prepareBillingAddress(?Address $address)
    {
        if (!empty($address)) {
            $this->addressCreator->setType($address, AddressType::BILLING_ADDRESS_TYPE);
        }
    }

    /**
     * @param $entity
     * @return string
     */
    private function prepareCustomerName($entity)
    {
        $name = '';

        if ($entity instanceof Company) {
            $name = $entity->getName();

        } elseif ($entity instanceof ContactPerson) {
            $name = $entity->getFirstName() . ' ' . $entity->getLastName();
        }

        return $name;
    }
}
