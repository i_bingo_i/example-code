<?php

namespace InvoiceBundle\Services\Customer\Creator;

use AppBundle\Entity\Address;
use AppBundle\Entity\ContactPerson;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Entity\EntityType;
use InvoiceBundle\Repository\EntityTypeRepository;
use InvoiceBundle\Services\Customer\CustomerManager;

class CustomerContactPersonCreator extends BaseCustomerCreator
{
    /**
     * CustomerContactPersonCreator constructor.
     * @param CustomerManager $customerManager
     * @param EntityTypeRepository $entityTypeRepository
     */
    public function __construct(
        CustomerManager $customerManager,
        EntityTypeRepository $entityTypeRepository
    ) {
        parent::__construct($customerManager, $entityTypeRepository);

        $this->entityType = $this->entityTypeRepository->findOneBy(['alias' => EntityType::TYPE_CONTACT_PERSON]);
    }

    /**
     * @param ContactPerson $contactPerson
     * @param string $name
     * @param Address $address
     * @return Customer
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createCustomer(ContactPerson $contactPerson, $name, ?Address $address = null)
    {
        $customer = parent::create($contactPerson->getId(), $name, $address);

        return $customer;
    }
}