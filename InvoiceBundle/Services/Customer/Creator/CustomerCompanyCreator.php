<?php

namespace InvoiceBundle\Services\Customer\Creator;

use AppBundle\Entity\Address;
use AppBundle\Entity\Company;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Entity\EntityType;
use InvoiceBundle\Repository\EntityTypeRepository;
use InvoiceBundle\Services\Customer\CustomerManager;

class CustomerCompanyCreator extends BaseCustomerCreator
{
    /**
     * CustomerCompanyBuilder constructor.
     * @param CustomerManager $customerManager
     * @param EntityTypeRepository $entityTypeRepository
     */
    public function __construct(
        CustomerManager $customerManager,
        EntityTypeRepository $entityTypeRepository
    ) {
        parent::__construct($customerManager, $entityTypeRepository);

        $this->entityType = $this->entityTypeRepository->findOneBy(['alias' => EntityType::TYPE_COMPANY]);
    }

    /**
     * @param Company $company
     * @param Address|null $address
     * @return \InvoiceBundle\Entity\Customer
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createCustomer(Company $company, ?Address $address = null)
    {
        $customer = parent::create($company->getId(), $company->getName(), $address);

        return $customer;
    }
}