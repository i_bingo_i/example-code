<?php

namespace InvoiceBundle\Services\Customer\Creator;

use AppBundle\Entity\Address;
use InvoiceBundle\Entity\EntityType;
use InvoiceBundle\Repository\EntityTypeRepository;
use InvoiceBundle\Factories\CustomerFactory;
use InvoiceBundle\Services\Customer\CustomerManager;

class BaseCustomerCreator
{
    /** @var CustomerManager */
    protected $customerManager;
    /** @var EntityTypeRepository */
    protected $entityTypeRepository;
    /** @var EntityType */
    protected $entityType;

    /**
     * BaseCustomerBuilder constructor.
     * @param CustomerManager $customerManager
     * @param EntityTypeRepository $entityTypeRepository
     */
    public function __construct(
        CustomerManager $customerManager,
        EntityTypeRepository $entityTypeRepository
    ) {
        $this->customerManager = $customerManager;
        $this->entityTypeRepository = $entityTypeRepository;
    }

    /**
     * @param $id
     * @param $name
     * @param Address $address
     * @return mixed
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function create($id, $name, ?Address $address = null)
    {
        $customer = CustomerFactory::make($id, $name, $this->entityType, $address);
        $this->customerManager->save($customer);

        return $customer;
    }
}