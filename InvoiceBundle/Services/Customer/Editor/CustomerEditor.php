<?php

namespace InvoiceBundle\Services\Customer\Editor;

use AppBundle\Entity\Address;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Services\Customer\CustomerManager;
use QuickbooksBundle\Services\Quickbooks\Customer\ModifierCustomer;

class CustomerEditor
{
    /** @var CustomerManager */
    private $customerManager;
    /** @var ModifierCustomer */
    private $modifierCustomer;

    /**
     * CustomerEditor constructor.
     * @param CustomerManager $customerManager
     * @param ModifierCustomer $modifierCustomer
     */
    public function __construct(CustomerManager $customerManager, ModifierCustomer $modifierCustomer)
    {
        $this->customerManager = $customerManager;
        $this->modifierCustomer = $modifierCustomer;
    }

    /**
     * @param Customer $customer
     * @param Address $address
     * @param $name
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function modify(Customer $customer, Address $address, $name)
    {
        if (!empty($customer->getAccountingSystemId()) && !empty($customer->getEditSequence())) {
            $this->editBillingAddressAndName($customer, $address, $name);
            $accountingSystemCustomer = $this->modifierCustomer->modifyCustomer($customer);
            $this->setAccountingSystemId($customer, $accountingSystemCustomer);

        } else {
            $this->editBillingAddressAndName($customer, $address, $name);

        }

        $this->customerManager->flush();
    }

    /**
     * @param Customer $customer
     * @param Address $address
     * @param string $name
     */
    public function editBillingAddressAndName(Customer $customer, Address $address, $name)
    {
        $customer->setBillingAddress($address);
        $customer->setName($name);
    }

    /**
     * @param Customer $customer
     * @param Address $address
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setCustomBillingAddress(Customer $customer, Address $address)
    {
        $customer->setCustomBillingAddress($address);

        $this->customerManager->save($customer);
    }

    /**
     * @param Customer $customer
     * @param Customer $customerFromAccountingSystem
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setAccountingSystemId(Customer $customer, Customer $customerFromAccountingSystem)
    {
        $customer->setAccountingSystemId($customerFromAccountingSystem->getAccountingSystemId());
        $customer->setEditSequence($customerFromAccountingSystem->getEditSequence());
    }
}