<?php

namespace InvoiceBundle\Services\Invoice;

use InvoiceBundle\Entity\Invoices;
use AppBundle\Entity\Repository\PaymentTermRepository;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use InvoiceBundle\DataTransportObject\InvoiceDTO;
use InvoiceBundle\Entity\Job;
use InvoiceBundle\Factories\InvoiceFactory;
use InvoiceBundle\Entity\InvoiceStatus;
use InvoiceBundle\Services\InvoiceStatus\InvoiceStatusProvider;
use InvoiceBundle\Services\Job\JobCreator;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class InvoiceCreator
{
    /** @var PaymentTermRepository */
    private $paymentTermRepository;
    /** @var TokenStorage */
    private $tokenStorage;
    /** @var User */
    private $currentUser;
    /** @var JobCreator */
    private $jobCreator;
    /** @var InvoiceStatusProvider */
    private $invoiceStatusProvider;

    /**
     * Creator constructor.
     * @param TokenStorage $tokenStorage
     * @param InvoiceStatusProvider $invoiceStatusProvider
     * @param PaymentTermRepository $paymentTermRepository
     * @param JobCreator $jobCreator
     */
    public function __construct(
        TokenStorage $tokenStorage,
        InvoiceStatusProvider $invoiceStatusProvider,
        PaymentTermRepository $paymentTermRepository,
        JobCreator $jobCreator
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->invoiceStatusProvider = $invoiceStatusProvider;
        $this->paymentTermRepository = $paymentTermRepository;
        $this->currentUser = $this->tokenStorage->getToken()->getUser();
        $this->jobCreator = $jobCreator;
    }

    /**
     * @param Workorder $workorder
     * @return Invoices
     */
    public function createBlankInvoice(Workorder $workorder)
    {
        /** @var InvoiceStatus $status */
        $status = $this->invoiceStatusProvider->getDraft();
        /** @var Job $job */
        $job = $this->jobCreator->createBlankJob($workorder);
        $invoiceDTO = new InvoiceDTO($workorder, $status, $this->currentUser, $job);

        return InvoiceFactory::make($invoiceDTO);
    }
}