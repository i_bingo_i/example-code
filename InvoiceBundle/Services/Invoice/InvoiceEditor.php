<?php

namespace InvoiceBundle\Services\Invoice;

use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Services\InvoiceStatus\InvoiceStatusProvider;
use InvoiceBundle\Services\PaymentTerm\PaymentTermProvider;

class InvoiceEditor
{
    /** @var PaymentTermProvider */
    private $paymentTermProvider;
    /** @var InvoiceStatusProvider */
    private $invoiceStatusProvider;

    /**
     * InvoiceEditor constructor.
     * @param PaymentTermProvider $paymentTermProvider
     * @param InvoiceStatusProvider $invoiceStatusProvider
     */
    public function __construct(
        PaymentTermProvider $paymentTermProvider,
        InvoiceStatusProvider $invoiceStatusProvider
    ) {
        $this->paymentTermProvider = $paymentTermProvider;
        $this->invoiceStatusProvider = $invoiceStatusProvider;
    }

    /**
     * @param Invoices $invoice
     * @param Invoices $invoiceFromRequest
     */
    public function edit(Invoices $invoice, Invoices $invoiceFromRequest)
    {
        $this->addLines($invoice, $invoiceFromRequest);
        $this->setBillingAddress($invoice, $invoiceFromRequest);
        $this->setPaymentTerm($invoice, $invoiceFromRequest);

        $invoice->setTotal($invoiceFromRequest->getTotal());
        $invoice->setPaymentApplied($invoiceFromRequest->getPaymentApplied());
        $invoice->setOverridePayment($invoiceFromRequest->getOverridePayment());
        $invoice->setBalanceDue($invoiceFromRequest->getBalanceDue());
    }

    /**
     * @param Invoices $invoices
     * @param Invoices $invFromAccSystem
     */
    public function modifyAfterSend(Invoices $invoices, Invoices $invFromAccSystem)
    {
        $invoices->setAccountingSystemId($invFromAccSystem->getAccountingSystemId());
        $invoices->setEditSequence($invFromAccSystem->getEditSequence());
        $invoices->setStatus($this->invoiceStatusProvider->getInQB());
    }

    /**
     * @param Invoices $invoice
     * @param Invoices $invoiceFromRequest
     */
    private function addLines(Invoices $invoice, Invoices $invoiceFromRequest)
    {
        if (!empty($invoiceFromRequest->getLines())) {
            foreach ($invoiceFromRequest->getLines() as $line) {
                $invoice->addLine($line);
            }
        }
    }

    /**
     * @param Invoices $invoice
     * @param Invoices $invoiceFromRequest
     */
    private function setBillingAddress(Invoices $invoice, Invoices $invoiceFromRequest)
    {
        if (!empty($invoiceFromRequest->getBillToAddress())) {
            $invoice->setBillToAddress($invoiceFromRequest->getBillToAddress());
        }
    }

    /**
     * @param Invoices $invoice
     * @param Invoices $invoiceFromRequest
     */
    private function setPaymentTerm(Invoices $invoice, Invoices $invoiceFromRequest)
    {
        $term = $this->paymentTermProvider->getByAlias($invoiceFromRequest->getPaymentTerm()->getAlias());

        if ($term) {
            $invoice->setPaymentTerm($term);
        }
    }
}