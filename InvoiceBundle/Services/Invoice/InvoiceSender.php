<?php

namespace InvoiceBundle\Services\Invoice;

use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Manager\InvoiceManager;
use InvoiceBundle\Services\InvoiceLine\InvoiceLineHandler;
use QuickbooksBundle\Services\Quickbooks\Invoice\CreatorInvoice;

class InvoiceSender
{
    /** @var CreatorInvoice */
    private $creatorInvoice;
    /** @var InvoiceEditor */
    private $invoiceEditor;
    /** @var InvoiceManager */
    private $invoiceManger;
    /** @var InvoiceLineHandler */
    private $invoiceLineHandler;

    /**
     * InvoiceSender constructor.
     * @param CreatorInvoice $creatorInvoice
     * @param InvoiceEditor $invoiceEditor
     * @param InvoiceManager $invoiceManger
     * @param InvoiceLineHandler $invoiceLineHandler
     */
    public function __construct(
        CreatorInvoice $creatorInvoice,
        InvoiceEditor $invoiceEditor,
        InvoiceManager $invoiceManger,
        InvoiceLineHandler $invoiceLineHandler
    ) {
        $this->creatorInvoice = $creatorInvoice;
        $this->invoiceEditor = $invoiceEditor;
        $this->invoiceManger = $invoiceManger;
        $this->invoiceLineHandler = $invoiceLineHandler;
    }

    /**
     * @param Invoices $invoice
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendToAccountingSystem(Invoices $invoice)
    {
        $invoiceFromAccountingSystem = $this->creatorInvoice->create($invoice);
        $this->invoiceEditor->modifyAfterSend($invoice, $invoiceFromAccountingSystem);
        $this->invoiceLineHandler->modifyLines($invoice, $invoiceFromAccountingSystem);

        $this->invoiceManger->flush();
    }
}