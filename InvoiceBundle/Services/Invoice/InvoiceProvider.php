<?php

namespace InvoiceBundle\Services\Invoice;

use AppBundle\Entity\Workorder;
use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Entity\InvoiceStatus;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;

class InvoiceProvider
{
    /** @var Serializer */
    private $serializer;

    /**
     * InvoiceProvider constructor.
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param $invoice
     * @return mixed|string
     */
    public function serializeInJSONInvoice(Invoices $invoice)
    {
        return $this->serializer->serialize(
            $invoice,
            'json',
            SerializationContext::create()->setGroups(['invoice'])
        );
    }

    /**
     * @param Invoices $invoice
     * @param string $invoiceJson
     * @return Invoices
     */
    public function deserializing(Invoices $invoice, $invoiceJson)
    {
        $context = DeserializationContext::create();
        $context->attributes->set('target', $invoice);
        return $this->serializer->deserialize($invoiceJson, Invoices::class, 'json', $context);
    }

    /**
     * @param Workorder $workorder
     * @return mixed
     */
    public function getDraftInvoice(Workorder $workorder)
    {
        $invoice = $workorder->getInvoices()->filter(function (Invoices $invoices) {
            return $invoices->getStatus()->getAlias() == InvoiceStatus::STATUS_DRAFT;
        })->first();

        return $invoice;
    }
}