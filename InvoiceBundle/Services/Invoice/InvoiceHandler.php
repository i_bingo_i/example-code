<?php

namespace InvoiceBundle\Services\Invoice;

use AppBundle\Entity\Workorder;
use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\Manager\InvoiceManager;
use InvoiceBundle\Services\InvoiceLine\InvoiceLineHandler;
use WorkorderBundle\Services\WorkorderEditor;

class InvoiceHandler
{
    /** @var InvoiceCreator */
    private $invoiceCreator;
    /** @var InvoiceProvider */
    private $invoiceProvider;
    /** @var InvoiceEditor */
    private $invoiceEditor;
    /** @var WorkorderEditor */
    private $workorderEditor;
    /** @var Invoices */
    private $invoice;
    /** @var InvoiceLineHandler */
    private $invoiceLineHandler;
    /** @var InvoiceManager */
    private $invoiceManger;

    public function __construct(
        InvoiceCreator $invoiceCreator,
        InvoiceProvider $invoiceProvider,
        InvoiceEditor $invoiceEditor,
        WorkorderEditor $workorderEditor,
        InvoiceLineHandler $invoiceLineHandler,
        InvoiceManager $invoiceManger
    ) {
        $this->invoiceCreator = $invoiceCreator;
        $this->invoiceProvider = $invoiceProvider;
        $this->invoiceEditor = $invoiceEditor;
        $this->workorderEditor = $workorderEditor;
        $this->invoiceLineHandler = $invoiceLineHandler;
        $this->invoiceManger = $invoiceManger;
    }

    /**
     * @param Workorder $workorder
     * @return Invoices
     */
    public function getInvoice(Workorder $workorder)
    {
        if (!$this->invoice = $this->invoiceProvider->getDraftInvoice($workorder)) {
            $this->invoice = $this->invoiceCreator->createBlankInvoice($workorder);
        }

        return $this->invoice;
    }

    /**
     * @param Workorder $workorder
     * @return mixed|string
     */
    public function getInvoiceInJsonFormat(Workorder $workorder)
    {
        $this->getInvoice($workorder);

        return $this->invoiceProvider->serializeInJSONInvoice($this->invoice);
    }

    /**
     * @param $invoiceJson
     * @return Invoices
     */
    public function saveInDB($invoiceJson)
    {
        /** @var Invoices $invoiceFromRequest */
        $invoiceFromRequest = $this->invoiceProvider->deserializing($this->invoice, $invoiceJson);

        //TODO Next four lines is bad practice. In previous line exists solution but it not work how it must.
        // In future we would fix it;
        $this->workorderEditor->editPoNoField($this->invoice->getWorkorder(), $invoiceFromRequest->getWorkorder());
        $this->invoiceEditor->edit($this->invoice, $invoiceFromRequest);
        $this->invoiceLineHandler->createInvoiceLines($this->invoice);

        $this->invoiceManger->save($this->invoice);

        return $this->invoice;
    }
}
