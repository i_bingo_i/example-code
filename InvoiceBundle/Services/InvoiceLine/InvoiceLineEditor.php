<?php

namespace InvoiceBundle\Services\InvoiceLine;

use InvoiceBundle\Entity\InvoiceLine;
use QuickbooksBundle\Repository\ItemRepository;

class InvoiceLineEditor
{
    private $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    /**
     * @param InvoiceLine $invoiceLine
     * @param $accountingSystemId
     */
    public function setAccountingSystemId(InvoiceLine $invoiceLine, $accountingSystemId)
    {
        $invoiceLine->setAccountingSystemId($accountingSystemId);
    }

    /**
     * @param InvoiceLine $line
     */
    public function editInvoiceLine(InvoiceLine $line)
    {
        $line->setItem($this->itemRepository->findOneBy(['qbId' => $line->getItem()->getQbId()]));
    }
}