<?php

namespace InvoiceBundle\Services\InvoiceLine;

use InvoiceBundle\Entity\InvoiceLine;
use InvoiceBundle\Entity\Invoices;

class InvoiceLineHandler
{
    /** @var InvoiceLineEditor */
    private $invoiceLineEditor;

    /**
     * InvoiceLineHandler constructor.
     * @param InvoiceLineEditor $invoiceLineEditor
     */
    public function __construct(InvoiceLineEditor $invoiceLineEditor)
    {
        $this->invoiceLineEditor = $invoiceLineEditor;
    }

    /**
     * @param Invoices $invoices
     * @param Invoices $invoiceFromAccountingSystem
     */
    public function modifyLines(Invoices $invoices, Invoices $invoiceFromAccountingSystem)
    {
        /** @var InvoiceLine $invoiceLine */
        foreach ($invoices->getLines() as $invoiceLine) {
            /** @var InvoiceLine $line */
            foreach ($invoiceFromAccountingSystem->getLines() as $line) {
                if ($invoiceLine->getItem()->getQbId() == $line->getItem()->getQbId()) {
                    $this->invoiceLineEditor->setAccountingSystemId($invoiceLine, $line->getAccountingSystemId());

                }
            }
        }
    }

    /**
     * @param Invoices $invoices
     */
    public function createInvoiceLines(Invoices $invoices)
    {
        /** @var InvoiceLine $line */
        foreach ($invoices->getLines() as $line) {
            $this->invoiceLineEditor->editInvoiceLine($line);
        }
    }
}