<?php

namespace InvoiceBundle\Services\InvoiceLine;

use Doctrine\Common\Persistence\ObjectManager;
use InvoiceBundle\Entity\InvoiceLine;

class InvoiceLineManager
{
    /** @var ObjectManager */
    private $objectManager;

    /**
     * InvoiceLineManager constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param InvoiceLine $invoiceLine
     */
    public function save(InvoiceLine $invoiceLine)
    {
        $this->objectManager->persist($invoiceLine);
        $this->objectManager->flush();
    }

    /**
     * @param InvoiceLine $invoiceLine
     */
    public function persist(InvoiceLine $invoiceLine)
    {
        $this->objectManager->persist($invoiceLine);
    }

    public function flush()
    {
        $this->objectManager->flush();
    }
}