<?php

namespace InvoiceBundle\Controller;

use AppBundle\Entity\EntityManager\WorkorderManager;
use InvoiceBundle\Entity\Invoices;
use AppBundle\Entity\Workorder;
use InvoiceBundle\Services\Customer\CustomerHandler;
use InvoiceBundle\Services\Customer\Editor\CustomerEditor;
use InvoiceBundle\Services\Invoice\InvoiceHandler;
use InvoiceBundle\Services\Invoice\InvoiceProvider;
use InvoiceBundle\Services\Invoice\InvoiceSender;
use InvoiceBundle\Services\Job\JobHandler;
use InvoiceBundle\Services\PaymentTerm\PaymentTermProvider;
use QuickbooksBundle\Services\Quickbooks\Customer\CreatorCustomer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WorkorderBundle\Services\WorkorderLogHandler;

class InvoiceController extends Controller
{
    /**
     * @param Workorder $workorder
     * @param Request $request
     * @return Response
     */
    public function createAction(Workorder $workorder, Request $request)
    {
        /** @var PaymentTermProvider $paymentTermProvider */
        $paymentTermProvider = $this->get('payment-term.provider');
        /** @var InvoiceHandler $invoiceHandler */
        $invoiceHandler = $this->get('invoice.handler');
        /** @var WorkorderLogHandler $workorderLogHandler */
        $workorderLogHandler = $this->get('workorder.log.handler');

        /** @var string $invoiceJson */
        $invoiceJson = $invoiceHandler->getInvoiceInJsonFormat($workorder);
        /** @var array $paymentTerms */
        $paymentTerms = $paymentTermProvider->getAll();

        if ($request->isMethod($request::METHOD_POST)) {
            $invoiceJson = $request->request->get('invoice');
            $invoiceHandler->saveInDB($invoiceJson);

            $workorderLogHandler->invoiceDraftWasSave($workorder);
        }

        return $this->render('InvoiceBundle::create.html.twig', [
            'invoice' => $invoiceJson,
            'terms' => $paymentTermProvider->serializeInJson($paymentTerms)
            ]);
    }

    /**
     * @param Invoices $invoice
     * @return Response
     */
    public function viewAction(Invoices $invoice)
    {
        /** @var InvoiceProvider $invoiceProvider */
        $invoiceProvider = $this->get('invoice.provider');

        $invoiceJson = $invoiceProvider->serializeInJSONInvoice($invoice);

        return $this->render('InvoiceBundle::view.html.twig', [
            'invoice' => $invoiceJson,
        ]);
    }

    /**
     * @param Workorder $workorder
     * @param Request $request
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function sendToAccountingSystemAction(Workorder $workorder, Request $request)
    {
        /** @var InvoiceHandler $invoiceHandler */
        $invoiceHandler = $this->get('invoice.handler');
        /** @var CustomerHandler $customerHandler */
        $customerHandler = $this->get('invoice.customer.handler');
        /** @var JobHandler $jobHandler */
        $jobHandler = $this->get('invoice.job.handler');
        /** @var InvoiceSender $invoiceSender */
        $invoiceSender = $this->get('invoice.sender');
        /** @var WorkorderLogHandler $workorderLogHandler */
        $workorderLogHandler = $this->get('workorder.log.handler');
        /** @var WorkorderManager $workorderManager */
        $workorderManager = $this->get('app.work_order.manager');

        /** @var string $invoiceJson */
        $invoiceJson = $request->request->get('invoice');

        $invoice = $invoiceHandler->getInvoice($workorder);
        $invoiceHandler->saveInDB($invoiceJson);

        $customerHandler->sendCustomerToAccountingSystem($invoice->getJob()->getCustomer());

        $jobHandler->sendJobToAccountingSystem($invoice->getJob());

        $invoiceSender->sendToAccountingSystem($invoice);

        $workorderManager->setStatus($workorder, $workorderManager::STATUS_SEND_INVOICE);

        $workorderLogHandler->invoiceSubmittedToAccountingSystem($workorder);
    }
}
