<?php

namespace InvoiceBundle\Controller\api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use InvoiceBundle\Services\ItemProvider;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use QuickbooksBundle\Services\App\Item\Synchronizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ItemRestController extends FOSRestController
{
    /**
     * Get Items
     *
     * ### Response OK ###
     *     {
     *           "id": "", (string)
     *           "name": "", (string)
     *           "description": "", (string)
     *           "type": { (object)
     *              "name": "" (string)
     *           },
     *           "rate": 0, (integer)
     *           "rateType": { (object)
     *              "name": "" (string)
     *           }
     *     },
     *     {
     *           "id": "", (string)
     *           "name": "", (string)
     *           "description": "", (string)
     *           "type": { (object)
     *              "name": "" (string)
     *           },
     *           "rate": 0, (integer)
     *           "rateType": { (object)
     *              "name": "" (string)
     *           }
     *     }
     *
     * @ApiDoc(
     *   section = "Item",
     *   tags={
     *     "real work method" = "#4890da"
     *   },
     *   resource = true,
     *   description = "Get Items",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     * @Rest\Get("/item")
     * @Rest\View(serializerGroups={"full"})
     * @return View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getItemsAction()
    {
        /** @var ItemProvider $itemProvider */
        $itemProvider = $this->get('invoice.item_provider');
        /** @var Synchronizer $synchronizer */
        $synchronizer = $this->get('quickbooks.item.synchronizer');

        $synchronizer->synchronizeItems();

        return $this->view($itemProvider->getAllItems(), Response::HTTP_OK);
    }
}