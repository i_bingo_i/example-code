<?php

namespace InvoiceBundle\DataFixtures;

use InvoiceBundle\Entity\EntityType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadEntityTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $contactPerson = new EntityType();
        $contactPerson->setName('Contact Person')
            ->setAlias('contact_person');

        $manager->persist($contactPerson);
        $this->addReference('entity_type_contact_person', $contactPerson);

        $company = new EntityType();
        $company->setName('Company')
            ->setAlias('company');

        $manager->persist($company);
        $this->addReference('entity_type_company', $company);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}