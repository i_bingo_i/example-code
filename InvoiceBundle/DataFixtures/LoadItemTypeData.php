<?php

namespace InvoiceBundle\DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use QuickbooksBundle\Entity\ItemType;

class LoadItemTypeData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $itemService = new ItemType();
        $itemService->setName('Service');
        $itemService->setAlias('service');

        $manager->persist($itemService);

        $itemOtherChargeRet = new ItemType();
        $itemOtherChargeRet->setName('Other Charge');
        $itemOtherChargeRet->setAlias('other_charge');

        $manager->persist($itemOtherChargeRet);

        $itemSubtotal = new ItemType();
        $itemSubtotal->setName('Subtotal');
        $itemSubtotal->setAlias('subtotal');

        $manager->persist($itemSubtotal);

        $itemDiscount = new ItemType();
        $itemDiscount->setName('Discount');
        $itemDiscount->setAlias('discount');

        $manager->persist($itemDiscount);

        $itemPayment = new ItemType();
        $itemPayment->setName('Payment');
        $itemPayment->setAlias('payment');

        $manager->persist($itemPayment);

        $itemGroupRet = new ItemType();
        $itemGroupRet->setName('Group');
        $itemGroupRet->setAlias('group');

        $manager->persist($itemGroupRet);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}