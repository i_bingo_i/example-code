<?php

namespace InvoiceBundle\DataTransportObject;

use AppBundle\Entity\AccountContactPerson;
use AppBundle\Entity\Address;
use AppBundle\Entity\User;
use AppBundle\Entity\Workorder;
use InvoiceBundle\Entity\Job;
use InvoiceBundle\Factories\ShipToAddressFactory;
use InvoiceBundle\Entity\InvoiceStatus;

class InvoiceDTO
{
    /** @var Workorder */
    private $workorder;
    /** @var AccountContactPerson */
    private $accountContactPerson;
    /** @var Address */
    private $billToAddress;
    /** @var InvoiceStatus */
    private $status;
    /** @var Address */
    private $shipToAddress;
    /** @var User */
    private $user;
    /** @var Job */
    private $job;

    /**
     * InvoiceDTO constructor.
     * @param Workorder $workorder
     * @param InvoiceStatus $status
     * @param User $user
     * @param Job $job
     */
    public function __construct(
        Workorder $workorder,
        InvoiceStatus $status,
        User $user,
        Job $job
    ) {
        $this->workorder = $workorder;
        $this->status = $status;
        $this->user = $user;
        $this->job = $job;
        $this->setAccountContactPerson();
        $this->setBillingAddress();
        $this->setShipToAddress();
    }

    /**
     * @return Workorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * @return AccountContactPerson
     */
    public function getAccountContactPerson()
    {
        return $this->accountContactPerson;
    }

    /**
     * @return Address
     */
    public function getBillToAddress()
    {
        return $this->billToAddress;
    }

    /**
     * @return InvoiceStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return Address
     */
    public function getShipToAddress()
    {
        return $this->shipToAddress;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     *
     */
    private function setAccountContactPerson()
    {
        $acp = $this->workorder->getAccount()->getContactPerson()->filter(function (AccountContactPerson $person) {
            return !empty($person->getPayment());
        })->first();

        $this->accountContactPerson = $acp;
    }

    /**
     */
    private function setBillingAddress()
    {
        if (!empty($this->accountContactPerson->getCustomer())) {
            $this->billToAddress = $this->accountContactPerson->getCustomer()->getBillingAddress();
        }
    }

    /**
     */
    private function setShipToAddress()
    {
        $this->shipToAddress = ShipToAddressFactory::make($this->workorder->getAccount());
    }
}