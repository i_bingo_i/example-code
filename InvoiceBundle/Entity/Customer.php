<?php

namespace InvoiceBundle\Entity;

use AppBundle\Entity\Address;

class Customer
{
    /** @var int */
    private $id;

    /** @var string */
    private $accountingSystemId;

    /** @var string */
    private $entityId;

    /** @var EntityType */
    private $entityType;

    /** @var Address */
    private $billingAddress;

    /** @var Address */
    private $customBillingAddress;

    /** @var string */
    private $name;

    /** @var string */
    private $editSequence;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAccountingSystemId()
    {
        return $this->accountingSystemId;
    }

    /**
     * @param string $accountingSystemId
     * @return $this
     */
    public function setAccountingSystemId($accountingSystemId = null)
    {
        $this->accountingSystemId = $accountingSystemId;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param string $entityId
     * @return $this
     */
    public function setEntityId($entityId = null)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * @return EntityType
     */
    public function getEntityType()
    {
        return $this->entityType;
    }

    /**
     * @param EntityType $entityType
     * @return $this
     */
    public function setEntityType($entityType = null)
    {
        $this->entityType = $entityType;

        return $this;
    }

    /**
     * @return Address
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param Address $billingAddress
     * @return $this
     */
    public function setBillingAddress($billingAddress = null)
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * @return Address
     */
    public function getCustomBillingAddress()
    {
        return $this->customBillingAddress;
    }

    /**
     * @param Address $customBillingAddress
     * @return $this
     */
    public function setCustomBillingAddress($customBillingAddress = null)
    {
        $this->customBillingAddress = $customBillingAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getEditSequence()
    {
        return $this->editSequence;
    }

    /**
     * @param string $editSequence
     */
    public function setEditSequence($editSequence)
    {
        $this->editSequence = $editSequence;
    }
}
