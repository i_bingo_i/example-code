<?php

namespace InvoiceBundle\Factories;

use AppBundle\Entity\Account;
use AppBundle\Entity\DeviceCategory;
use InvoiceBundle\Entity\Customer;
use InvoiceBundle\Entity\Job;

class JobFactory
{
    /**
     * @param Account $account
     * @param Customer $customer
     * @param DeviceCategory $category
     * @return Job
     */
    public static function make(
        Account $account,
        Customer $customer,
        DeviceCategory $category
    ) {
        $job = new Job();

        return $job->setAccount($account)
            ->setCustomer($customer)
            ->setDivision($category);
    }
}
