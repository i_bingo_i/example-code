<?php

namespace InvoiceBundle\Factories;

use InvoiceBundle\Entity\Invoices;
use InvoiceBundle\DataTransportObject\InvoiceDTO;

class InvoiceFactory
{
    /**
     * @param InvoiceDTO $invoiceDTO
     * @return Invoices
     */
    public static function make(InvoiceDTO $invoiceDTO)
    {
        $invoice = new Invoices();
        $invoice->setAccountContactPerson($invoiceDTO->getAccountContactPerson());
        $invoice->setWorkorder($invoiceDTO->getWorkorder());
        $invoice->setUser($invoiceDTO->getUser());
        $invoice->setStatus($invoiceDTO->getStatus());
        $invoice->setBillToAddress($invoiceDTO->getBillToAddress());
        $invoice->setShipToAddress($invoiceDTO->getShipToAddress());
        $invoice->setJob($invoiceDTO->getJob());

        return $invoice;
    }
}